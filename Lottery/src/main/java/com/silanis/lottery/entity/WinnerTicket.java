package com.silanis.lottery.entity;

public class WinnerTicket extends Ticket{

	private float prize;
	private int position;
	
	public WinnerTicket(){}
	
	public WinnerTicket(Ticket ticket){
		
		this.setFirstName(ticket.getFirstName());
		this.setBallNumber(ticket.getBallNumber());
		this.setDraw(ticket.getDraw());
		
	}
	
	public float getPrize() {
		return prize;
	}
	public void setPrize(float prize) {
		this.prize = prize;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "WinnerTicket [prize=" + prize + ", position=" + position + ", toString()=" + super.toString() + "]";
	}
	
	
	
}

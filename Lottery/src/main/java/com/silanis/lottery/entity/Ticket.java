package com.silanis.lottery.entity;

public class Ticket {

	private int ballNumber;
	
	private Draw draw;
	
	private String firstName;

	public int getBallNumber() {
		return ballNumber;
	}

	public void setBallNumber(int ballNumber) {
		this.ballNumber = ballNumber;
	}

	public Draw getDraw() {
		return draw;
	}

	public void setDraw(Draw draw) {
		this.draw = draw;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "Ticket [ballNumber=" + ballNumber + ", draw=" + draw + ", firstName=" + firstName + "]";
	}
	
	
	
}

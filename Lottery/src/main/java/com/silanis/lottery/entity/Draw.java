package com.silanis.lottery.entity;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.silanis.lottery.exceptions.DrawAlreadExecutedException;
import com.silanis.lottery.exceptions.DrawNotExcutedException;
import com.silanis.lottery.exceptions.FirstNameAlreadyExistsException;
import com.silanis.lottery.exceptions.InsufficientNumberTicketsToDraw;
import com.silanis.lottery.exceptions.InvalidFirstNameException;


/**
 * Stores and Manages all the related information about the draw
 * @author damar
 *
 */

public class Draw {
	
	public static final int NUMBER_OF_WINNERS_PER_DRAW = 3;
	public static final float PRICE_PER_TICKET = 10f;
	public static final float[] PRIZE_FACTOR_ARRAY = new float[]{0.75f,0.15f,0.1f}; 
	public static final float TOTAL_PRIZE_POT_RATIO = 0.5f;
	public static final int MINIMUM_TICKETS_DRAW = 5;
	
	private final Map<String,Ticket> ticketMap = new HashMap<>();
	private final ArrayList<Integer> winnerBalls = new ArrayList<>(NUMBER_OF_WINNERS_PER_DRAW);

	private int currentBallNumber = 1;
	
	private final float initialPotValue;
	
	public Draw(float initialPotValue){
		this.initialPotValue = initialPotValue; 
	}
	
	public Ticket purchase(String firstName) throws FirstNameAlreadyExistsException, InvalidFirstNameException {
		
		
		if (firstName == null || firstName.trim().isEmpty()){
			throw new InvalidFirstNameException();
		}

		String trimmedFirstName = firstName.trim(); 
		
		if (ticketMap.containsKey(trimmedFirstName)){
			throw new FirstNameAlreadyExistsException();
		}
		
		Ticket ticket = new Ticket();
		ticket.setFirstName(trimmedFirstName);
		ticket.setBallNumber(currentBallNumber++);
		ticket.setDraw(this);
		ticketMap.put(trimmedFirstName,ticket);
		
		return ticket;
	}
	
	
	public Ticket getTicket(String firstName){
		
		return ticketMap.get(firstName);
	}
	
	public int getNumberOfTicketsSold(){
		
		return ticketMap.size();
	}
	
	public List<Ticket> getTicketList(){
		return new ArrayList<Ticket>(ticketMap.values());
	}
	
	
	public List<Integer> draw() throws InsufficientNumberTicketsToDraw, DrawAlreadExecutedException{

		if (winnerBalls.size() > 0){
			throw new DrawAlreadExecutedException();
		}
		
		if (ticketMap.size() < MINIMUM_TICKETS_DRAW){
			throw new InsufficientNumberTicketsToDraw();
		}
		 
		for (int i=0;i<NUMBER_OF_WINNERS_PER_DRAW;i++){
			winnerBalls.add(getUniqueRadomNumber());
		}
		return new ArrayList<Integer>(winnerBalls);
	}
	
	
	public List<WinnerTicket> getWinners() throws DrawNotExcutedException{
		
		if (winnerBalls.size() == 0){
			throw new DrawNotExcutedException(); 
		}
		
		List<WinnerTicket> winnersList = new ArrayList<>(NUMBER_OF_WINNERS_PER_DRAW);
		
		for (Ticket ticket : ticketMap.values()){
			int indexOfTicket = winnerBalls.indexOf(ticket.getBallNumber()); 
			if (indexOfTicket != -1){
				WinnerTicket winner = new WinnerTicket(ticket);
				winner.setPosition(indexOfTicket + 1);
				winner.setPrize(calculatePrize(indexOfTicket));
				winnersList.add(winner);
				ticketMap.put(ticket.getFirstName(), winner);
			}
		}
		return winnersList;
	}
	
	protected float calculatePrize(int position){
		
		return getTotalPrize() * PRIZE_FACTOR_ARRAY[position] ;
		
	}
	
	public float getTotalPot(){
		
		return ticketMap.size() * PRICE_PER_TICKET + initialPotValue;
	}
	
	public float getTotalPrize(){
		
		return getTotalPot() * TOTAL_PRIZE_POT_RATIO;
	}
	
	protected int getUniqueRadomNumber(){
		
		int number = generateRandomNumber();
		if (winnerBalls.contains(number)){
			number = getUniqueRadomNumber();
		}
		return number;
	}
	
	
	
	protected int generateRandomNumber(){
		
		int generatedNumber = -1;
		try {
			
			SecureRandom numberGenerator = SecureRandom.getInstanceStrong();
			int numberTicketsSold = getNumberOfTicketsSold();
			generatedNumber = numberGenerator.nextInt(numberTicketsSold);
			generatedNumber ++;
		} catch (NoSuchAlgorithmException e) {
			// TODO: thrown exception here
		}		
		return generatedNumber;
	}
	
	
}

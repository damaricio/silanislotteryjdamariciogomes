package com.silanis.lottery.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.silanis.lottery.entity.Draw;
import com.silanis.lottery.entity.Ticket;
import com.silanis.lottery.entity.WinnerTicket;
import com.silanis.lottery.exceptions.DrawAlreadExecutedException;
import com.silanis.lottery.exceptions.DrawNotExcutedException;
import com.silanis.lottery.exceptions.FirstNameAlreadyExistsException;
import com.silanis.lottery.exceptions.InsufficientNumberTicketsToDraw;
import com.silanis.lottery.exceptions.InvalidFirstNameException;
import com.silanis.lottery.facade.LoterryFacade;
import com.silanis.lottery.facade.LotteryFacadaImpl;

public class MainDisplay {

	private LoterryFacade loterryFacade = new LotteryFacadaImpl();
	
	public static final String[] PRIZE_POSITION_STRING = new String[] {"First", "Second", "Third"};

	private LoteryTicketTable loteryTicketTable;
	
	private JLabel lblDrawInformation;

	public void createAndShowGUI() {

		JFrame frame = new JFrame("Silanis Lotery");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(900, 700);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
		frame.getContentPane().setLayout(new BorderLayout());

		loteryTicketTable = new LoteryTicketTable();
		frame.getContentPane().add(createHeader(), BorderLayout.NORTH);
		frame.getContentPane().add(createTableSection(), BorderLayout.CENTER);
		
		frame.setVisible(true);

		updateUI();
		
	}

	private JPanel createTableSection() {
		JPanel center = new JPanel();
		center.setBackground(Color.BLUE);
		center.setLayout(new BorderLayout());

		JScrollPane tableScrollPane = new JScrollPane(loteryTicketTable);
		center.add(tableScrollPane, BorderLayout.CENTER);
		return center;
	}

	private JPanel createHeader() {

		final JPanel header = new JPanel();
		header.setBackground(Color.GREEN);

		final JLabel lblFistName = new JLabel("Fist Name");
		header.add(lblFistName);

		final JTextField txtFirstName = new JTextField();
		txtFirstName.setPreferredSize(new Dimension(150, 26));
		header.add(txtFirstName);

		final JButton btnPurchase = new JButton("Purchase");
		header.add(btnPurchase);
		btnPurchase.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					loterryFacade.purchase(txtFirstName.getText());
					updateUI();
				} catch (FirstNameAlreadyExistsException fnae) {
					JOptionPane.showMessageDialog(null, "First name already exists!");
				} catch (InvalidFirstNameException ifne){
					JOptionPane.showMessageDialog(null, "Invalid first name!");
				}

			}
		});

		final JButton btnDraw = new JButton("Draw");
		btnDraw.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					List<Integer> winnerBalls = loterryFacade.draw();
					updateUI();
					String message = String.format("The winner ball were: First: %d, Second: %d, Third: %d",winnerBalls.get(0),winnerBalls.get(1),winnerBalls.get(2));
					JOptionPane.showMessageDialog(null, message);
				} catch (InsufficientNumberTicketsToDraw itde){
					JOptionPane.showMessageDialog(null, "Isuffuent Tickets to Draw, the minimum tickets are : "+Draw.MINIMUM_TICKETS_DRAW);
				} catch (DrawAlreadExecutedException dae){
					JOptionPane.showMessageDialog(null, "Draw already executed exception, create a new draw");
				}
			}
		});
		header.add(btnDraw);

		final JButton btnWinners = new JButton("Winners");
		btnWinners.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					loterryFacade.winners();
					updateUI();
				} catch (DrawNotExcutedException dnee){
					JOptionPane.showMessageDialog(null, "Before get the winners execute the draw!");
				}
			}
		});
		header.add(btnWinners);
		
		final JButton btnCreateNewDraw = new JButton("Create a New Draw");
		btnCreateNewDraw.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				loterryFacade.createDraw();
				updateUI();
			}
		});
		header.add(btnCreateNewDraw);
		
		lblDrawInformation = new JLabel("");
		header.add(lblDrawInformation);

		return header;
	}
	
	private void updateUI(){
		lblDrawInformation.setText(String.format("Current draw prize: %10.2f",loterryFacade.getCurrentDraw().getTotalPrize()));
		loteryTicketTable.updateUI();
	}

	public static void main(String[] args) {

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				(new MainDisplay()).createAndShowGUI();
			}
		});
	}

	public class LoteryTicketTable extends JTable  {
		String[] columnNames = new String[] { "Ball Number", "First Name", "Winner Position", "Prize Value" };

		public LoteryTicketTable() {

			setModel(new DefaultTableModel() {

				@Override
				public Object getValueAt(int rowIndex, int columnIndex) {
					Ticket ticket = loterryFacade.getCurrentDraw().getTicketList().get(rowIndex);
					Object value = "";
					switch (columnIndex) {
					case 0:
						value = ticket.getBallNumber();
						break;
					case 1:
						value = ticket.getFirstName();
						break;
					case 2:
						if (ticket instanceof WinnerTicket) {
							value = PRIZE_POSITION_STRING[((WinnerTicket) ticket).getPosition() -1];
						}
						break;
					case 3:
						if (ticket instanceof WinnerTicket) {
							value = (new DecimalFormat("CA$ #.00")).format(((WinnerTicket) ticket).getPrize());
						}
						break;
					default:
						break;
					}
					return value;
				}

				@Override
				public int getRowCount() {
					return loterryFacade.getCurrentDraw().getNumberOfTicketsSold();
				}

				@Override
				public int getColumnCount() {
					return 4;
				}

				@Override
				public String getColumnName(int index) {
					return columnNames[index];
				}
			});

		}

	}

}

package com.silanis.lottery.facade;

import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;


import com.silanis.lottery.entity.Draw;
import com.silanis.lottery.entity.WinnerTicket;
import com.silanis.lottery.exceptions.DrawAlreadExecutedException;
import com.silanis.lottery.exceptions.DrawNotExcutedException;
import com.silanis.lottery.exceptions.FirstNameAlreadyExistsException;
import com.silanis.lottery.exceptions.InsufficientNumberTicketsToDraw;
import com.silanis.lottery.exceptions.InvalidFirstNameException;


public class LotteryFacadaImpl implements LoterryFacade {

	static Logger log = LogManager.getLogger(LotteryFacadaImpl.class);
	
	
	private final List<Draw> drawList = new LinkedList<>();
	private final float initialPot = 200f;
	
	
	public LotteryFacadaImpl(){
		Configurator.setRootLevel(Level.DEBUG);
		// creates a initial draw with the initial total value
		log.debug("loading LotteryFacade with initialPot: "+initialPot);
		createDraw(initialPot);
		
	}
	
	@Override
	public void purchase(String firstName) throws FirstNameAlreadyExistsException,InvalidFirstNameException  {
		log.debug("Purchase for: "+firstName);
		getCurrentDraw().purchase(firstName);
	}
	
	/**
	 * Gets the current active draw
	 * @return
	 */
	@Override
	public Draw getCurrentDraw(){
		log.debug("the current draw is: "+drawList.size());

		return drawList.get(drawList.size()-1);
	}
	
	@Override
	public void createDraw(){
		// creates a new draw with what is left from the previous draw
		createDraw(getCurrentDraw().getTotalPrize());
	}
	
	private void createDraw(float initialPotValue){
		log.debug("creating a new draw: "+initialPotValue);
		Draw draw = new Draw(initialPotValue);
		drawList.add(draw);
	}
	
	@Override
	public List<Integer> draw() throws InsufficientNumberTicketsToDraw,DrawAlreadExecutedException {
		List<Integer> drawList = getCurrentDraw().draw();
		log.debug("executting the draw: "+drawList);
		return drawList;
	}

	@Override
	public List<WinnerTicket> winners() throws DrawNotExcutedException {
		List<WinnerTicket> winners = getCurrentDraw().getWinners();
		log.debug("the winners are: "+winners);
		return winners;
	}
	
}

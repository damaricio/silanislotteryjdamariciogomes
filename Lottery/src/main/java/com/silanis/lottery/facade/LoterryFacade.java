package com.silanis.lottery.facade;

import java.util.List;



import com.silanis.lottery.entity.Draw;
import com.silanis.lottery.entity.WinnerTicket;
import com.silanis.lottery.exceptions.DrawAlreadExecutedException;
import com.silanis.lottery.exceptions.DrawNotExcutedException;
import com.silanis.lottery.exceptions.FirstNameAlreadyExistsException;
import com.silanis.lottery.exceptions.InsufficientNumberTicketsToDraw;
import com.silanis.lottery.exceptions.InvalidFirstNameException;

/**
 * This a facade interface for all the Silanis Services
 * @author damar
 *
 */

public interface LoterryFacade {

	/**
	 * Create and ad a ticket with a specific FirstName for the current draw
	 * @param firstName
	 * @throws FirstNameAlreadyExistsException
	 * @throws InvalidFirstNameException
	 */
	void purchase(String firstName) throws FirstNameAlreadyExistsException, InvalidFirstNameException;

	/**
	 * Returns the current active draw
	 * @return
	 */
	Draw getCurrentDraw();

	/**
	 * Draws the winner 3 balls 
	 * @return
	 * @throws InsufficientNumberTicketsToDraw :number of tickets are less than 5
	 * @throws DrawAlreadExecutedException : I only can execute the draw once
	 */
	List<Integer> draw() throws InsufficientNumberTicketsToDraw, DrawAlreadExecutedException;
	
	/**
	 * Apply the winner balls to the tickets placing prize and position 
	 * @return
	 * @throws DrawNotExcutedException :  you can only get the winners if the draw was executed
	 */
	List<WinnerTicket> winners() throws DrawNotExcutedException;

	void createDraw();

}
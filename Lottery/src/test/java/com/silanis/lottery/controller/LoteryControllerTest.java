package com.silanis.lottery.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.silanis.lottery.entity.Draw;
import com.silanis.lottery.entity.Ticket;
import com.silanis.lottery.entity.WinnerTicket;
import com.silanis.lottery.exceptions.DrawAlreadExecutedException;
import com.silanis.lottery.exceptions.DrawNotExcutedException;
import com.silanis.lottery.exceptions.FirstNameAlreadyExistsException;
import com.silanis.lottery.exceptions.InsufficientNumberTicketsToDraw;
import com.silanis.lottery.exceptions.InvalidFirstNameException;
import com.silanis.lottery.facade.LoterryFacade;
import com.silanis.lottery.facade.LotteryFacadaImpl;

public class LoteryControllerTest {
	
	
	LoterryFacade loterryFacade = new LotteryFacadaImpl();
	
	@Test(expected=FirstNameAlreadyExistsException.class)
	public void testPurchaseFistNameAlreadyExists() throws FirstNameAlreadyExistsException, InvalidFirstNameException {
		
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		String firstName = "TestName";
		loterryFacade.purchase(firstName);
		loterryFacade.purchase(firstName);
	}

	@Test(expected=InvalidFirstNameException.class)
	public void testPurchaseInvalidFistNameEmpty() throws FirstNameAlreadyExistsException, InvalidFirstNameException {
		
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		String firstName = "";
		loterryFacade.purchase(firstName);
	}
	
	@Test(expected=InvalidFirstNameException.class)
	public void testPurchaseInvalidFistNameNull() throws FirstNameAlreadyExistsException, InvalidFirstNameException {
		
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		String firstName = null;
		loterryFacade.purchase(firstName);
	}
	
	
	
	@Test
	public void testSinglePurchase() throws FirstNameAlreadyExistsException, InvalidFirstNameException{
		
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		
		String firstName = "TestName";
		
		loterryFacade.purchase(firstName);
		
		Ticket ticket = loterryFacade.getCurrentDraw().getTicket(firstName);
		
		assertNotNull(ticket);
		
		assertEquals(ticket.getFirstName(), firstName);
		assertEquals(ticket.getBallNumber(), 1);
		
	}
	
	@Test (expected=InsufficientNumberTicketsToDraw.class)
	public void testDrawInsufficentTickets() throws FirstNameAlreadyExistsException, InvalidFirstNameException, InsufficientNumberTicketsToDraw, DrawAlreadExecutedException{
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		
		final int numberOfTicketsSold = Draw.MINIMUM_TICKETS_DRAW -1;
		String firstName = "TestName";

		for (int i=0;i<numberOfTicketsSold;i++)
			loterryFacade.purchase(firstName+"_"+i);
		
		List<Integer> winnerBalls = loterryFacade.draw();
		
	}

	@Test (expected=DrawAlreadExecutedException.class)
	public void testDrawAlreadExecutedException() throws FirstNameAlreadyExistsException, InvalidFirstNameException, InsufficientNumberTicketsToDraw, DrawAlreadExecutedException{
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		
		final int numberOfTicketsSold = Draw.MINIMUM_TICKETS_DRAW;
		String firstName = "TestName";

		for (int i=0;i<numberOfTicketsSold;i++)
			loterryFacade.purchase(firstName+"_"+i);
		
		List<Integer> winnerBalls = loterryFacade.draw();

		// Confirming if the first call was done 
		assertNotNull(winnerBalls);
		assertEquals(3, winnerBalls.size());
		
		loterryFacade.draw();
	}
	
	@Test
	public void testDrawWith5Tickets() throws FirstNameAlreadyExistsException, InvalidFirstNameException, InsufficientNumberTicketsToDraw, DrawAlreadExecutedException{
		
		
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		
		final int numberOfTicketsSold = 5;
		String firstName = "TestName";

		for (int i=0;i<numberOfTicketsSold;i++)
			loterryFacade.purchase(firstName+"_"+i);
		
		List<Integer> winnerBalls = loterryFacade.draw();
		
		assertNotNull(winnerBalls);
		
		assertEquals(3, winnerBalls.size());
		
		List<Integer> alreadyDrawBalls = new ArrayList<>();
		
		for (Integer winnerBall : winnerBalls){

			assertNotNull(winnerBall);
			assertTrue(winnerBall >= 0);
			assertTrue(winnerBall <= numberOfTicketsSold);
			assertTrue(!alreadyDrawBalls.contains(winnerBall));
			alreadyDrawBalls.add(winnerBall);
		}
	}
	
	@Test
	public void testGetWinnersSuccess() throws FirstNameAlreadyExistsException, InvalidFirstNameException, InsufficientNumberTicketsToDraw, DrawNotExcutedException, DrawAlreadExecutedException{
		
		final int numberOfTicketsSold = 5;
		
		LoterryFacade loterryFacade = new LotteryFacadaImpl();
		
		String firstName = "TestName";

		for (int i=0;i<numberOfTicketsSold;i++)
			loterryFacade.purchase(firstName+"_"+i);
		
		loterryFacade.draw();
		
		List<WinnerTicket> winners = loterryFacade.winners();
		
		assertNotNull(winners);
		
		assertEquals(3, winners.size());
		
		for (WinnerTicket winner : winners){

			assertNotNull(winner);
			assertTrue(winner.getBallNumber() >= 0);
			assertTrue(winner.getBallNumber() <= numberOfTicketsSold);
		}
	}

}
